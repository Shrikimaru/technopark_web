# coding=utf-8
import sys
sys.path.append("/home/roman/ask_pupkin")
from urlparse import parse_qs

def application(environ, start_response):
    status = '200 OK'

    output = '''
	    <meta charset="utf8">
	    GET параметры: 
    '''
    
    GET = parse_qs(str(environ.get('QUERY_STRING')))
    for key in GET:
        output += key + " => " + str(GET[key]) + ", "

    output += "<br>"

    output += "POST параметры: "

    try:
        request_body_size = int(environ.get('CONTENT_LENGTH', 0))
    except (ValueError):
        request_body_size = 0

    request_body = environ['wsgi.input'].read(request_body_size)
    query_list = parse_qs(request_body)

    for key in query_list:
    	output += key + " => " + str(query_list[key]) + ", "

    response_headers = [('Content-type', 'text/html'), ("Content-length", str(len(output)))]
    start_response(status, response_headers)
    return [output]