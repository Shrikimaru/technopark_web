from django.conf.urls import include, url, patterns
from django.contrib import admin

urlpatterns = [
    url(r'^$', 'ask_roman_app.views.index'),
    url(r'login', 'ask_roman_app.views.auth'),
    url(r'signup', 'ask_roman_app.views.signup'),
    url(r'question', 'ask_roman_app.views.questionPage'),
    url(r'logout', "ask_roman_app.views.disauth"),
    url(r'ask', "ask_roman_app.views.createQuestion"),
    url(r'profile', "ask_roman_app.views.settingsPage"),
    url(r'vote', "ask_roman_app.views.voteQuestion"),
    url(r'mark_right', "ask_roman_app.views.markAsTrue")
]