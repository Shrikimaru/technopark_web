# coding=utf-8
from django import forms

import sys
reload(sys)
sys.setdefaultencoding('utf-8')



TEXT_FIELD_LENGTH = 40

class AuthForm(forms.Form):
    login = forms.CharField(max_length=TEXT_FIELD_LENGTH)
    password = forms.CharField(max_length=TEXT_FIELD_LENGTH, widget=forms.PasswordInput)

class RegisterForm(forms.Form):
    login = forms.CharField(max_length=TEXT_FIELD_LENGTH)
    password = forms.CharField(max_length=TEXT_FIELD_LENGTH, widget=forms.PasswordInput)
    repeatedPassword = forms.CharField(max_length=TEXT_FIELD_LENGTH, widget=forms.PasswordInput)
    email = forms.EmailField()
    avatar = forms.ImageField()

    def clean(self):
        if self.cleaned_data.get('password') != self.cleaned_data.get('repeatedPassword'):
            raise forms.ValidationError('Пароли должны совпадать!')
        return self.cleaned_data

class ProfileChangeForm(forms.Form):
    newLogin = forms.CharField(max_length=TEXT_FIELD_LENGTH, required=False)
    newPassword = forms.CharField(max_length=TEXT_FIELD_LENGTH, required=False, widget=forms.PasswordInput)
    repeatedNewPassword = forms.CharField(max_length=TEXT_FIELD_LENGTH, required=False, widget=forms.PasswordInput)
    newEmail = forms.EmailField(required=False)
    newAvatar = forms.ImageField(required=False)

    def clean(self):
        if self.cleaned_data.get('newPassword') != self.cleaned_data.get('repeatedNewPassword'):
            raise forms.ValidationError('Пароли должны совпадать!')
        return self.cleaned_data

class AnswerAddForm(forms.Form):
    answer = forms.CharField(widget=forms.Textarea)

class QuestionAddForm(forms.Form):
    title = forms.CharField(max_length=255)
    content = forms.CharField(widget=forms.Textarea)
    tags = forms.CharField(required=False)