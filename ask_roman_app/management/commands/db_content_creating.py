# coding=utf-8
from django.core.management.base import BaseCommand
import sys
from mixer import fakers as f
import random
import hashlib
import csv
from ask_roman_app.models import *

reload(sys)
sys.setdefaultencoding('utf-8')

def generateFakeUserInformation():
    nickname = f.get_username()
    email = f.get_email(nickname)
    # password = hashlib.sha512("1").hexdigest()
    password = "testpass"
    rating = random.randint(1, 1000)
    return ("", email, nickname, password, "", "", rating)

def generateFakeQuestionInformation():
    title = f.get_lorem(250)
    content = f.get_lorem(500)
    author_id = random.randint(1,30)
    created_date = "2014-11-03"
    return ("", title, content, author_id, created_date, "")

def generateFakeAnswerInformation():
    content = f.get_lorem(1000)
    author = random.randint(1, 30)
    question = random.randint(1, 100)
    right = 0
    date = "2014-11-03"
    return ("", content, author, question, date, right)

def generateFakeTagsToQuestionInformation():
    tagId = random.randint(1, 100)
    questionId = random.randint(1, 100)
    return ("", tagId, questionId)

def writeFakeInformationInCSV(f, *args):
    writer = csv.writer(f, delimiter=",", quotechar='"', quoting=csv.QUOTE_NONNUMERIC, lineterminator='\n')
    writer.writerow(args[0])

class Command(BaseCommand):
    args = 'no arguments'
    help = """
    Создает 4 файла .csv в корне проекта: users, questions, answers и tagstoquestion,
    а так же заполняет базу пользователей через модель
    """

    def handle(self, *args, **options):
        fAns = open("answers.csv", "w")
        fQue = open("questions.csv", "w")
        fUse = open("users.csv", "w")
        fTtq = open("tagsToQuestions.csv", "w")

        for i in xrange(0, 30):
            info = generateFakeUserInformation()
            newUser = User.objects.create_user(info[2], info[1], info[3])
            u = MyUser(user_id = newUser.id, rating = info[6])
            u.save()
            writeFakeInformationInCSV(fUse, generateFakeUserInformation())
            print "Создано пользователей: "+str(i)

        for i in xrange(0, 100):
            writeFakeInformationInCSV(fQue, generateFakeQuestionInformation())
            print "Создано вопросов: "+str(i)

        for i in xrange(0, 200):
            writeFakeInformationInCSV(fAns, generateFakeAnswerInformation())
            print "Создано ответов: "+str(i)

        for i in xrange(0, 1000):
            writeFakeInformationInCSV(fTtq, generateFakeTagsToQuestionInformation())
            print "Создано тегов к вопросам: "+str(i)

        fAns.close()
        fQue.close()
        fUse.close()
        fTtq.close()