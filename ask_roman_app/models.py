from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class MyUser(models.Model):
    user = models.OneToOneField(User)
    avatar = models.ImageField(upload_to="avatars/", default="troll.png")
    rating = models.FloatField()

class Tag(models.Model):
    tag_name = models.CharField(max_length=50)
    tag_weight = models.FloatField()

class Question(models.Model):
    title = models.CharField(max_length=300)
    content = models.TextField()
    author = models.ForeignKey(MyUser)
    created_date = models.DateTimeField(auto_now=True)
    votes = models.IntegerField()

class Answer(models.Model):
    content = models.TextField()
    author = models.ForeignKey(MyUser)
    question = models.ForeignKey(Question)
    date = models.DateTimeField(auto_now=True)
    is_right = models.BooleanField(default=False)

class TagsToQuestion(models.Model):
    tagId = models.ForeignKey(Tag)
    questionId = models.ForeignKey(Question)

class VotesToQuestion(models.Model):
    questionId = models.ForeignKey(Question)
    userVotedId = models.ManyToManyField(User)