# coding=utf-8
from urlparse                   import parse_qs
from django.http                import HttpResponse
from django.core.paginator      import Paginator, PageNotAnInteger
from django.contrib.auth        import authenticate, login, logout
from django.contrib.auth.models import User
from django.core.exceptions     import ObjectDoesNotExist
from ask_roman_app              import models
from ask_roman_app.functions    import *
from ask_roman_app.forms        import *
import re
import sys
import json

reload(sys)
sys.setdefaultencoding('utf-8')


MESSAGES_CONTENT = {
    0: "",
    1: "Вы не авторизованы",
    2: "Авторизация не удалась",
    3: "Вы успешно зарегистрировались",
    4: "Вы авторизованы!",
    5: "Данные успешно изменены",
    6: "Регистрация не удалась...",
    7: "Не удалось изменить данные",
    8: "Не удалось добавить вопрос",
    9: "Вопрос успешно добавлен",
    10: "Не удалось добавить ответ",
    11: "Ответ успешно добавлен",
    12: "Такого вопроса не существует",
    13: "Такой пользователь уже существует",
    14: "Такой email уже существует"
}

MESSAGES_TYPES = {
    0: "",
    1: "message-success",
    2: "message-error"
}

# Create your views here.
class methodRequestParser:
    def __init__(self, METHOD):
        __methodRequestVars = {}
        self._methodRequestParser__methodRequestVars = parse_qs(METHOD.urlencode())
    def getListOfVars(self):
        return self._methodRequestParser__methodRequestVars

class question:
    questionContent_title = ""
    questionContent_description = ""
    numOfAnswers = 0
    votes = 0
    tags = 0
    authorNickname = ""
    authorAvatar = ""
    questionId = 0
    def __init__(self, p_tags, p_votes, p_numOfAnswers, p_question, p_authorInfo, p_id):
        self.questionContent_title = p_question['title']
        self.questionContent_description = p_question["description"]
        self.numOfAnswers = p_numOfAnswers
        self.votes = p_votes
        self.tags = p_tags
        self.authorNickname = p_authorInfo['nickname']
        self.authorAvatar = p_authorInfo['avatar']
        self.questionId = p_id

# Тестовая вьюшка для первого ДЗ
def home(request):
    GET = methodRequestParser(request.GET).getListOfVars()
    POST = methodRequestParser(request.POST).getListOfVars()
    answer = "GET: "
    for key in GET:
        answer += key + " = " + GET[key].pop() + "; "
    answer += "<br>"
    answer += "POST: "
    for key in POST:
        answer += key + " = " + POST[key].pop() + "; "

    response = HttpResponse(answer)
    response['Content-type'] = "text/html"

    return response


# Вьюшка для отображения главной страницы вопросов
def index(request):
    popularTags = ["c++", "oop", "python", "iphone", "PHP", "html", "javascript"];

    db_obj = models.Question.objects.all().order_by("-created_date", "votes")

    paginator = Paginator(db_obj, 5)
    page = request.GET.get("page")
    try:
        p = paginator.page(page)
    except PageNotAnInteger:
        p = paginator.page(1)

    q = []

    for questionObj in p:

        tagsConnections = models.TagsToQuestion.objects.filter(questionId__exact=questionObj.id)
        numOfAnswers = models.Answer.objects.filter(question__exact=questionObj.id).count()
        questionContent = {"title": questionObj.title, "description" : questionObj.content}
        authorInfo = {"nickname": questionObj.author.user.username, "avatar": questionObj.author.avatar}
        q.append(question(tagsConnections, questionObj.votes, numOfAnswers, questionContent, authorInfo, questionObj.id))

    return renderWithMessage(
        request,
        "index.html",
        getMessageFromRequest(request, MESSAGES_CONTENT),
        getMessageTypeFromRequest(request, MESSAGES_TYPES),
        {
            "questions":q,
            "popularTags":popularTags,
            "paginator":p
        },
        content_type="text/html"
    )

def questionPage(request):

    message = getMessageFromRequest(request, MESSAGES_CONTENT)
    message_type = getMessageTypeFromRequest(request, MESSAGES_TYPES)
    form = AnswerAddForm()

    if request.method == "POST":
        if not request.user.is_authenticated():
            return redirectWithMessage(request.META['HTTP_REFERER'], 1, 2)
        else:
            form = AnswerAddForm(request.POST)
            if form.is_valid():
                answer = models.Answer(
                    content =form.cleaned_data['answer'],
                    author_id = request.user.myuser.id,
                    question_id = request.POST['question_id']
                )
                answer.save()
                message = MESSAGES_CONTENT[11]
                message_type = MESSAGES_TYPES[1]
            else:
                message = MESSAGES_CONTENT[10]
                message_type = MESSAGES_TYPES[2]

    try:
        questionId = int(request.GET.get("id", 0))
    except ValueError:
        return redirectWithMessage(request.META['HTTP_REFERER'], 12, 2)

    question = models.Question.objects.get(id=questionId)
    answersDbObj = models.Answer.objects.filter(question=questionId)
    paginator = Paginator(answersDbObj, 5)
    page = request.GET.get("page")
    try:
        answers = paginator.page(page)
    except PageNotAnInteger:
        answers = paginator.page(1)

    tagsOfQuestion = models.TagsToQuestion.objects.filter(questionId__exact=question.id)
    popularTags = ["c++", "oop", "python", "iphone", "PHP", "html", "javascript"]

    return renderWithMessage(
        request,
        "question.html",
        message,
        message_type,
        {
            "form" : form,
            "question":question,
            "tagsOfQuestion":tagsOfQuestion,
            "answers":answers,
            "popularTags":popularTags,
            "id":questionId
        }
    )

def auth(request):
    if request.method == "POST":
        form = AuthForm(request.POST)
        if form.is_valid():
            user = authenticate(username=form.cleaned_data['login'], password=form.cleaned_data['password'])
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return redirectWithMessage("/", 4, 1)
                else:
                    return renderWithMessage(
                        request,
                        "login.html",
                        MESSAGES_CONTENT[2],
                        MESSAGES_TYPES[2],
                        {"form" : form}
                    )
            else:
                return renderWithMessage(
                    request,
                    "login.html",
                    MESSAGES_CONTENT[2],
                    MESSAGES_TYPES[2],
                    {"form" : form}
                )
        else:
            return render(request, "login.html", {"form" : form}, content_type="text/html")
    else:
        if not request.user.is_authenticated():
            return renderWithMessage(
                request,
                "login.html",
                getMessageFromRequest(request, MESSAGES_CONTENT),
                getMessageTypeFromRequest(request, MESSAGES_TYPES),
                {"form" : AuthForm()}
            )
        else:
            return redirectWithMessage(request.META['HTTP_REFERER'], 4, 1)


def disauth(request):
    logout(request)
    return redirect(request.META['HTTP_REFERER'])


def signup(request):
    if request.method == "POST":
        form = RegisterForm(request.POST, request.FILES)
        if form.is_valid():

            username = form.cleaned_data['login']
            try:
                u = models.MyUser.objects.get(user__username=username)
                return renderWithMessage(
                    request,
                    "signup.html",
                    MESSAGES_CONTENT[13],
                    MESSAGES_TYPES[2],
                    {"form" : form})
            except models.MyUser.DoesNotExist:
                pass

            email = form.cleaned_data['email']
            try:
                u = models.MyUser.objects.get(user__email=email)
                return renderWithMessage(
                    request,
                    "signup.html",
                    MESSAGES_CONTENT[14],
                    MESSAGES_TYPES[2],
                    {"form" : form})
            except models.MyUser.DoesNotExist:
                pass

            password = form.cleaned_data['password']
            newUser = User.objects.create_user(username, email, password)
            profile = models.MyUser(rating = 0, user_id = newUser.id)
            profile.avatar = request.FILES['avatar']
            profile.save()
            return redirectWithMessage("/login", 3, 1)
        else:
            return renderWithMessage(
                request,
                "signup.html",
                MESSAGES_CONTENT[6],
                MESSAGES_TYPES[2],
                {"form" : form}
            )
    else:
        return render(request, "signup.html", {"form" : RegisterForm()}, content_type="text/html")

def createQuestion(request):
    if not request.user.is_authenticated():
        return redirectWithMessage(request.META['HTTP_REFERER'], 1, 2)
    else:
        if request.method == "POST":
            form = QuestionAddForm(request.POST)
            if form.is_valid():
                question = models.Question(
                    title = form.cleaned_data['title'],
                    content = form.cleaned_data['content'],
                    author_id = request.user.myuser.id,
                    votes = 0
                )
                question.save()
                tags = form.cleaned_data['tags']
                tags = tags.replace(" ", "")
                if tags != "":
                    tags = tags.split(',')
                    for tag in tags:
                        try:
                            dbTag = models.Tag.objects.get(tag_name = tag)
                            tt = models.TagsToQuestion(tagId_id = dbTag.id, questionId_id = question.id)
                            tt.save()
                        except ObjectDoesNotExist:
                            t = models.Tag(tag_name = tag, tag_weight = 0)
                            t.save()
                            tt = models.TagsToQuestion(tagId_id = t.id, questionId_id = question.id)
                            tt.save()
                return redirectWithMessage("/question?id="+str(question.id), 9, 1)
            else:
                return renderWithMessage(
                    request,
                    "createQuestion.html",
                    MESSAGES_CONTENT[8],
                    MESSAGES_TYPES[2],
                    {"form" : form}
                )
        else:
            return renderWithMessage(
                request,
                "createQuestion.html",
                getMessageFromRequest(request, MESSAGES_CONTENT),
                getMessageTypeFromRequest(request, MESSAGES_TYPES),
                {"form" : QuestionAddForm()})

def settingsPage(request):
    if not request.user.is_authenticated():
        return redirectWithMessage(request.META['HTTP_REFERER'], 1, 2)
    else:
        if request.method == "POST":
            form = ProfileChangeForm(request.POST, request.FILES)
            if form.is_valid():

                if form.cleaned_data['newLogin']:
                    request.user.username = form.cleaned_data['newLogin']

                if form.cleaned_data['newEmail']:
                    request.user.email = form.cleaned_data['newEmail']

                if form.cleaned_data['newAvatar']:
                    profile = request.user.myuser
                    profile.avatar = request.FILES['newAvatar']
                    profile.save()

                if form.cleaned_data['newPassword']:
                    request.user.set_password(form.cleaned_data['newPassword'])
                    request.user.save()

                return renderWithMessage(
                    request,
                    "profile.html",
                    MESSAGES_CONTENT[5],
                    MESSAGES_TYPES[1],
                    {"form" : ProfileChangeForm()},
                    content_type="text/html"
                )
            else:
                return renderWithMessage(
                    request,
                    "profile.html",
                    MESSAGES_CONTENT[7],
                    MESSAGES_TYPES[2],
                    {"form" : form},
                    content_type="text/html"
                )
        else:
            return render(
                request,
                "profile.html",
                {"form" : ProfileChangeForm()},
                content_type="text/html"
            )

def voteQuestion(request):

    if not request.user.is_authenticated():
        ans = {"status" : 0, "message" : "Вы не авторизованы"}
        ans = json.dumps(ans)
        return HttpResponse(ans, content_type="application/json")

    vote = request.POST.get('type')
    if( vote == "down" ):
        vote = -1
    if( vote == "up" ):
        vote = 1
    questionId = int(request.POST.get('id'))
    userId = request.user.id

    try:
        hasVoted = models.VotesToQuestion.objects.get(questionId=questionId, userVotedId=userId)
    except models.VotesToQuestion.DoesNotExist:
        q = models.Question.objects.get(id=questionId)
        q.votes = newRating = q.votes + vote
        q.save()

        v = models.VotesToQuestion(questionId=q)
        v.save()
        v.userVotedId.add(request.user)
        v.save()

        ans = {"status" : 1, "new_rating" : newRating}
        ans = json.dumps(ans)
        return HttpResponse(ans, content_type="application/json")

    ans = {"status" : 0, "message" : "Вы уже голосовали за этот вопрос"}
    ans = json.dumps(ans)
    return HttpResponse(ans, content_type="application/json")

def markAsTrue(request):
    if not request.user.is_authenticated():
        ans = json.dumps({"status" : 0, "message" : MESSAGES_CONTENT[1]})
        return HttpResponse(ans, content_type="application/json")
    else:
        user_id = request.user.id
        ans_id = request.POST.get("ans_id", 0)
        answer = models.Answer.objects.get(id=ans_id)
        creator_id = answer.question.author.user.id
        if user_id != creator_id:
            ans = json.dumps({"status" : 0, "message" : "Вы не являетесь создателем вопроса"})
            return HttpResponse(ans, content_type="application/json")
        else:
            try:
                isRightThere = models.Answer.objects.get(is_right=True, question_id=answer.question.id)
                isRightThere.is_right = False
                isRightThere.save()
            except models.Answer.DoesNotExist:
                pass
            answer.is_right = True
            answer.save()
            ans = json.dumps({"status" : 1})
            return HttpResponse(ans, content_type="application/json")