from django.shortcuts import redirect, render

def redirectWithMessage(address, message, message_type):
    if address.find("?") == -1 :
        return redirect(address+"?message="+str(message)+"&message_type="+str(message_type))
    else:
        return redirect(address+"&message="+str(message)+"&message_type="+str(message_type))

def renderWithMessage(request, template, message, message_type, args, content_type = "text/html"):
    parameters = {"message" : message, "message_type" : message_type}
    for key in args:
        parameters[key] = args[key]
    return render(
        request,
        template,
        parameters,
        content_type=content_type
    )

def getMessageFromRequest(request, dict):
    numOfMessage = int(request.GET.get("message", 0))
    return dict[numOfMessage]

def getMessageTypeFromRequest(request, dict):
    numOfMessageType = int(request.GET.get("message_type", 0))
    return dict[numOfMessageType]